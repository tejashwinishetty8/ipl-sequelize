const sequelize = require('../model/sequelizeSetUp');
const Sequelize = require('sequelize');

const matches = sequelize.define('matches', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    season: Sequelize.STRING,
    date: Sequelize.DATE,
    team1: Sequelize.STRING,
    team2: Sequelize.STRING,
    toss_winner: Sequelize.STRING,
    result: Sequelize.STRING,
    winner: Sequelize.STRING,
    win_by_runs: Sequelize.INTEGER,
    win_by_wickets: Sequelize.INTEGER
});

const deliveries = sequelize.define('deliveries', {
    id: {
        type: Sequelize.INTEGER(10),
        primaryKey: true
    },
    batting_team: Sequelize.STRING,
    bowling_team: Sequelize.STRING,
    over: Sequelize.INTEGER,
    ball: Sequelize.INTEGER,
    batsman: Sequelize.STRING,
    bowler: Sequelize.STRING,
    batsman_runs: Sequelize.INTEGER,
    extra_runs: Sequelize.INTEGER,
    total_runs: Sequelize.INTEGER
});

//matches.belongsTo(deliveries, { foreignKey: 'id' });
deliveries.removeAttribute("id");

deliveries.belongsTo(matches, { foreignKey: 'match_id' });
matches.hasMany(deliveries, { foreignKey: 'id' });

sequelize.sync();

module.exports = {
    matches: matches,
    deliveries: deliveries
}